import React, { Component } from "react";
import Flex from "./components/container";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div>
        <h1>To do list</h1>

        <Flex />
      </div>
    );
  }
}

export default App;
