import React, { Component } from "react";
import "../App.css";

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = { inputText: "" };
  }

  handleChange = (e) => {
    this.setState({ inputText: e.target.value }); //handleChange wali value local state par save hogi
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.addTask(this.state.inputText); //handleSubmit wali value parent state par jayegi

    this.setState({ inputText: "" });
  };

  //form mei enter karoge toh wo submit mana jaega aur value server mei jaane ki koshish karegi
  render() {
    return (
      <form type="submit" onSubmit={this.handleSubmit}>
        <input type="checkbox" className="checkbox" />
        <input
          type="text"
          placeholder="Create a new todo..."
          value={this.state.inputText}
          onChange={this.handleChange}
          className="input_box"
        />
      </form>
    );
  }
}

export default Input;
