import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import Bottom from "./bottom";
import Input from "./input";
import "../App.css";

class Flex extends Component {
  constructor(props) {
    super(props);

    this.state = { tasks: [], activeFilter: "all" };
    //tasks is an array of objects
  }

  handleAddTask = (text) => {
    // console.log(text)

    let newTask = {
      text: text,
      id: uuidv4(),
      isCompleted: false,
    };

    this.setState({ tasks: [newTask, ...this.state.tasks] });
  };

  //let arr= this.state.tasks and then arr.push() --> this is wrong method becz arr now holds
  // reference to same array and changes will get add to same array but basically we want to
  // create something fresh so react is able to identify changes properly.

  handleTaskToggle = (id) => {
    let updated_array = this.state.tasks.map((object) => {
      if (object.id === id)
        return {
          ...object,

          isCompleted: !object.isCompleted,
        };
      else return object;
    }); //map

    this.setState({ tasks: updated_array });

    console.log(this.state.tasks);
  };

  handleRemove = (id) => {
    this.setState({
      tasks: this.state.tasks.filter((object) => {
        return object.id !== id;
      }),
    });
  };

  getFilteredTasks = () => {
    if (this.state.activeFilter === "all") return this.state.tasks;
    else if (this.state.activeFilter === "active")
      return this.state.tasks.filter((obj) => !obj.isCompleted);
    else if (this.state.activeFilter === "completed")
      return this.state.tasks.filter((obj) => obj.isCompleted);
    else return [];
  };

  handleClearCompleted = () => {
    let arr = this.state.tasks.filter((task) => {
      return !task.isCompleted;
    });

    this.setState({ tasks: arr });
  };

  //to update my state based on button clicked

  handleSetFilter = (filter_value) => {
    this.setState({ activeFilter: filter_value });
  };

  render() {
    let display = this.getFilteredTasks();

    let counter = this.state.tasks.filter((todo) => {
      return !todo.isCompleted;
    }).length;

    return (
      <div className="main-flex">
        <div className="center-flex">
          <Input addTask={this.handleAddTask} />

          <ul>
            {display.map((item) => {
              return (
                <li
                  style={{ display: "flex", padding: "1rem" }}
                  key={item.id}
                  className={item.isCompleted ? "checked" : ""}
                >
                  <form>
                    <input
                      type="checkbox"
                      className="checkbox"
                      checked={item.isCompleted}
                      onChange={() => {
                        this.handleTaskToggle(item.id);
                      }}
                    />
                  </form>

                  {item.text}

                  <button
                    onClick={() => {
                      this.handleRemove(item.id);
                    }}
                  >
                    X
                  </button>
                </li>
              ); //return end
            })}
          </ul>

          <Bottom
            filterValue={this.handleSetFilter}
            tasks={this.state.tasks}
            handleClearCompleted={this.handleClearCompleted}
            counter={counter}
          />
        </div>
      </div>
    );
  } //render
}
export default Flex;

















/* This code was creating problem - display is undefined in next file (this.props.display)

         <Add 
          display={this.display}
          taskToggle= {this.handleTaskToggle}
          remove={this.handleRemove}
         
         
         />
*/

/*

  handleAll = (e) => {
    this.setState({ data: this.main_array });
  };

  handleActive = (e) => {
    let active_array = this.state.data.filter((todo) => {
      return todo.isCompleted === false;
    });

    this.setState({ data: active_array });
  };

  handleCompleted = (e) => {
    let completed_array = this.main_array.filter((todo) => {
      return todo.isCompleted === true;
    });

    this.setState({ data: completed_array });

    

  }

  handleClearCompleted = (e) => {
    let updatedArray = [];

    this.main_array.forEach((todo) => {
      if (todo.isCompleted === false) {
        updatedArray.push(todo);

        let main_arr = this.main_array.filter((todo) => {
          return todo.isCompleted === false;
        });

        this.main_array = main_arr;
      }
    });

    this.setState({
      data: updatedArray,
    });
  }

  

  handleCheckbox = (e, item) => {
    if (e.target.checked) {
      e.target.parentElement.classList.add("checked");
      item.isCompleted = item.isCompleted ? false : true;
      this.setState({ data: this.state.data });

      this.count--;
      this.update_counter();
    } else {
      e.target.parentElement.classList.remove("checked");
      item.isCompleted = item.isCompleted ? false : true;
      this.setState({ data: this.state.data });

      this.count++;
      this.update_counter();
    }
  };

  update_counter() {
    let counter = document.getElementById("counter");

    if (this.count === 0) {
      counter.textContent = "No items left";
    } else {
      counter.textContent = this.count + " items left";
    }
  }

  handleDeleteButton = (e) => {
    e.target.parentElement.remove();
    this.count--;
    this.update_counter();
  };
}

export default Flex;



*/

// import React, { Component } from 'react';
// import Input from './input';

// class Container extends Component {

//     render() {
//         return (

//            <Input/>

//         );
//     }
// }

// export default Container;
